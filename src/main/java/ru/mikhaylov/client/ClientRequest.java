package ru.mikhaylov.client;

import java.io.IOException;
import java.io.StringReader;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import static ru.mikhaylov.client.ClientException.*;

public class ClientRequest {

	private Client client;
	private String requestType;

	private ClientRequest() {
	}

	private ClientRequest(Client client, String requestType) {
		this.client = client;
		this.requestType = requestType;
	}

	public Client getClient() {
		return client;
	}

	public String getRequestType() {
		return requestType;
	}
	
	public static ClientRequest createClientRequestFromXml(String input) throws ClientException {
		try {
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(input));
			Document doc = db.parse(is);

			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			XPathExpression expr = xpath.compile("/request/request-type/text()");

			NodeList nl = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			String requestType = nl.item(0).getTextContent();

			expr = xpath.compile("/request/extra[@name='login']/text()");
			nl = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			String login = nl.item(0).getTextContent();

			expr = xpath.compile("/request/extra[@name='password']/text()");
			nl = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			String password = nl.item(0).getTextContent();

			Client client = new Client(login, password);
			ClientRequest clientRequest = new ClientRequest(client, requestType);
			return clientRequest;
		} catch (ParserConfigurationException | SAXException | IOException | NullPointerException | XPathExpressionException e) {
			throw new ClientException("bad message format", ERROR_TECHNICAL);
		}
	}

	public static String getClientRequestResponse(int code) {
		String formatString = "<?xml version='1.0' encoding='utf-8'?>\n<response>\n<result-code>%d</result-code>\n</response>";
		return (String.format(formatString,code));		
	}

	public static String getClientRequestResponse(int code, Double balance) {	
		String formatString = "<?xml version='1.0' encoding='utf-8'?>\n<response>\n<result-code>%d</result-code>\n<extra name='balance'>%1.2f</extra>\n</response>";
		return (String.format(Locale.UK,formatString,code,balance));
	}



}
