package ru.mikhaylov.client;

public class ClientException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int code;

	public final static int OK = 0;
	public final static int ERROR_USER_ALREADY_EXISTS = 1;
	public final static int ERROR_TECHNICAL = 2;
	public final static int ERROR_USER_NOT_EXISTS = 3;
	public final static int ERROR_PASSWORD_WRONG = 4;

	public int getCode() {
		return code;
	}

	public ClientException(String message, int code) {
		super(message);
		this.code = code;
	}
}
