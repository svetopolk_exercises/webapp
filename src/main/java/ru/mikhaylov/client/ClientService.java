package ru.mikhaylov.client;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import org.apache.log4j.Logger;

import static ru.mikhaylov.client.ClientException.*;

public class ClientService {

	final static Logger log = Logger.getLogger(ClientService.class);
	
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver"); // it doesn't work under tomcat without it
			log.info("load jdbc driver manually");
		} catch (ClassNotFoundException e) {
			log.error(e.getStackTrace());
		}
	} 

	public int insertClient(Client client) {
		Connection con = createConnection();
		try {
			insertClient(con, client);
			return 0; // it's ok
		} catch (ClientException e) {
			return e.getCode(); // return error code
		} finally {
			closeConnection(con);
		}
	}

	public double getClientBalance(Client client) throws ClientException {
		Connection con = createConnection();
		try {
			double resultBalance = getClientBalance(con, client);
			return resultBalance;
		} finally {
			closeConnection(con);
		}
	}

	private int insertClient(Connection con, Client client) throws ClientException {
		if (con == null)
			throw new ClientException("unknown error", ERROR_TECHNICAL);
		if (checkExistClient(con, client)) {
			log.debug("client [" + client.getLogin() + "] already exists in the DB, normal mode");
			throw new ClientException("user already exists with the same login", ERROR_USER_ALREADY_EXISTS);
		}
		try {
			PreparedStatement preparedStatement = con.prepareStatement("insert into CLIENT (login,password) values (?, ?)");
			preparedStatement.setString(1, client.getLogin());
			preparedStatement.setString(2, client.getPassword());
			preparedStatement.executeUpdate();
			con.commit();
		} catch (SQLIntegrityConstraintViolationException e) { // there is unique constrain on the data base
			log.debug("client [" + client.getLogin() + "] already exists in the DB, normal mode, race condition");
			throw new ClientException("user already exists with the same login", ERROR_USER_ALREADY_EXISTS);
		} catch (SQLException e) {
			log.error(e.getStackTrace());
			throw new ClientException("unknown error", ERROR_TECHNICAL);
		}
		System.out.println("insert is ok");
		return 0; // insert is ok
	}

	private double getClientBalance(Connection con, Client client) throws ClientException {
		if (con == null)
			throw new ClientException("no connection", ERROR_TECHNICAL);

		if (!checkExistClient(con, client))
			throw new ClientException("user does not exist", ERROR_USER_NOT_EXISTS);

		try {
			PreparedStatement preparedStatement = con.prepareStatement("select login,password,balance from CLIENT where login=?");
			preparedStatement.setString(1, client.getLogin());
			ResultSet rs = preparedStatement.executeQuery();
			rs.next();
			String password = rs.getString("password");
			if (!client.getPassword().equals(password))
				throw new ClientException("password is wrong", ERROR_PASSWORD_WRONG);
			double balance = rs.getDouble("balance");
			log.trace("balance="+balance);
			return balance;
		} catch (SQLException e) {
			log.error(e.getStackTrace());
			throw new ClientException("sql exception", ERROR_TECHNICAL);
		}
	}

	private boolean checkExistClient(Connection con, Client client) throws ClientException {
		try {
			PreparedStatement preparedStatement = con.prepareStatement("select login from CLIENT where login=?");
			preparedStatement.setString(1, client.getLogin());
			ResultSet result = preparedStatement.executeQuery();
			boolean clientExist = result.first(); // true - if first record exists, false - if returns nothing
			return clientExist;
		} catch (SQLException e) {
			log.error(e.getStackTrace());
			throw new ClientException("sql exception", ERROR_TECHNICAL);
		}
	}

	private Connection createConnection() {
		Connection c = null;
		try {
			c = DriverManager.getConnection("jdbc:mysql://localhost:3306/evator?autoReconnect=true&useSSL=false", "root", "lazer2");
			c.setAutoCommit(false);
		} catch (SQLException e) {
			log.error("error on DB connection creation");
			log.error(e.getStackTrace());
		}
		return c;
	}

	private void closeConnection(Connection con) {
		try {
			if (con != null)
				con.close();
		} catch (SQLException e) {
			log.error("error on DB connection close");
			log.error(e.getStackTrace());
		}
	}

}