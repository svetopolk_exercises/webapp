package ru.mikhaylov.client;

public class Client {

	private String login;
	private String password;

	public Client(String login, String password) {
		super();
		this.login = login;
		this.password = password;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}
}
