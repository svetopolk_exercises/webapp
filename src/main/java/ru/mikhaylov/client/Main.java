package ru.mikhaylov.client;

import java.util.Locale;

import org.apache.log4j.Logger;

public class Main {
	final static Logger log = Logger.getLogger(Main.class);

	public static void main(String args[]) {

		log.info("start main");

		// Client client = new Client("ivan1", "pass");
		// ClientDAO clientDAO = new ClientDAO();
		// System.out.println("1) результат вставки клиента " + client.getLogin() + " такой: " + clientDAO.insertClient(client));
		//
		// client = new Client("ivan2", "pass");
		// try {
		// System.out.println("2) баланс клиента " + client.getLogin() + " такой: " + clientDAO.getClientBalance(client));
		// } catch (ClientException e) {
		// System.out.println("2) ошибка на сервере при запросе баланса клиента: " + client.getLogin() + " такая: " + e.getCode() + " " + e.getMessage());
		// }
		//
		// client = new Client("ivan2", "pass1");
		// try {
		// System.out.println("3) баланс клиента " + client.getLogin() + " такой: " + clientDAO.getClientBalance(client));
		// } catch (ClientException e) {
		// System.out.println("3) ошибка на сервере при запросе баланса клиента: " + client.getLogin() + " такая: " + e.getCode() + " " + e.getMessage());
		// }
		//
		// client = new Client("ivan4", "pass1");
		// try {
		// System.out.println("4) баланс клиента " + client.getLogin() + " такой: " + clientDAO.getClientBalance(client));
		// } catch (ClientException e) {
		// System.out.println("4) ошибка на сервере при запросе баланса клиента: " + client.getLogin() + " такая: " + e.getCode() + " " + e.getMessage());
		// }

//		String input = "<?xml version='1.0' encoding='utf-8'?><request><request-type>CREATE-AGT</request-type><extra name='login'>123456</extra><extra name='password'>pwd</extra></request>";
//		ClientRequest clientRequest=null;
//		try {
//			clientRequest = ClientRequest.createClientRequestFromXml(input);
//			System.out.println(clientRequest.getClient().getLogin()+clientRequest.getClient().getPassword()+clientRequest.getRequestType());
//		} catch (ClientException e) {
//			 System.out.println("5) плохой формат сообщения");
//		}
		
		int code = 1;
		double balance=100;
		String formatString = "<?xml version='1.0' encoding='utf-8'?>\n<response>\n<result-code>%d</result-code>\n<extra name='balance'>%1.2f</extra>\n</response>";
		System.out.println( (String.format(Locale.UK,formatString,code,balance)));;
		
	}
}
