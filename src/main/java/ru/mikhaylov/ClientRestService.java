package ru.mikhaylov;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import ru.mikhaylov.client.*;
import static ru.mikhaylov.client.ClientException.*;

/**
 * Root resource (exposed at "clientrestservice" path)
 */
@Path("clientrestservice")
public class ClientRestService {

	final static Logger log = Logger.getLogger(ClientRestService.class);
			
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getIt() {
		return "It is GET, use POST";
	}

	@POST
	@Consumes(MediaType.TEXT_XML + ";charset=UTF-8")
	@Produces(MediaType.TEXT_XML + ";charset=UTF-8")
	public String postIt(String inputXml) {
		log.debug("1 message in");
		ClientRequest clientRequest;
		try {
			clientRequest = ClientRequest.createClientRequestFromXml(inputXml);
		} catch (ClientException e) {
			return ClientRequest.getClientRequestResponse(e.getCode()); //can't read xml
		}

		if (clientRequest.getRequestType().equals("CREATE-AGT")) {
			int resultCode = new ClientService().insertClient(clientRequest.getClient());
			return ClientRequest.getClientRequestResponse(resultCode);
		} else if (clientRequest.getRequestType().equals("GET-BALANCE")) {
			Double balance;
			try {
				balance = new ClientService().getClientBalance(clientRequest.getClient());
			} catch (ClientException e) {
				return ClientRequest.getClientRequestResponse(e.getCode());
			}
			return ClientRequest.getClientRequestResponse(OK, balance);
		} else {	// unknown request-type
			return ClientRequest.getClientRequestResponse(ERROR_TECHNICAL); 
		}
	}

}
